An ecommerce storefront for my wife Grace to sell her handmade products.

This is a demo version.

Built with AngularJS, AngularUI, Bootstrap, LESS, CSS3, and JQuery.

[My Portfolio](http://jakelee.io)