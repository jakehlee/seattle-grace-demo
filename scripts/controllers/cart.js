'use strict';

angular.module('seattleGrace')
  .controller('Cart', CartCtrl);

CartCtrl.$inject = ['$modal'];

function CartCtrl($modal){
  var vm = this;
  vm.cartItems = [
    {
      "id": 111,
      "sku" : "111LVSC",
      "name" : "Lavender & Violet Square Centerpiece",
      "image" : "c1.jpg",
      "images" : ["c1a.jpg","c1b.jpg","c1c.jpg"],
      "summary" : "A relaxing lavender centerpiece with flower pot",
      "description": "The colors and style of this centerpiece are sure to bring calm and relaxation to any space. Complete with a matching square porcelain flower pot, this one is not to be missed. ",
      "price" : 119.99,
      "discount" : 10,
      "inventory" : 22,
      "details" : ["1 large rose", "2 medium roses"],
      "size" : " 8H x 7W x 3D ",
      "category" : "preserved florals",
      "subcategory" : "centerpieces",
      "new" : true
    },
    {
      "id": 123,
      "sku" : "123MRB",
      "name" : "Monogrammed in Roses Box",
      "image" : "f3.jpg",
      "images" : ["f3a.jpg","f3b.jpg","f3c.jpg"],
      "summary" : "A monogrammed arrangement of roses for that special someone.",
      "description": "This is the perfect gift for that special someone. More personal and unique than any other gift of flowers. Be sure to customize your own now.",
      "price" : 199.99,
      "discount" : 5,
      "inventory" : 1,
      "details" : ["25-40 small roses"],
      "size" : " 8H x 8W x 4D ",
      "category" : "preserved florals",
      "subcategory" : "framed"
    },
    {
      "id" : 132,
      "sku" : "132FGW",
      "name" : "Fresh Greens Wreath",
      "image" : "w2.jpg",
      "summary" : "A fresh and clean wreath embodying spring",
      "description": "This wreath resembles a piece of a secret elven forest. Refreshing, clean, and full of life, it is sure to add freshness and new life to any space.",
      "price" : 89.99,
      "discount" : 15,
      "inventory" : 33,
      "details" : ["3 ounces of lilacs"],
      "size" : " 8H x 7W x 3D ",
      "category" : "preserved florals",
      "subcategory" : "wreaths"
    },
    {
      "id": 122,
      "sku" : "122CSWC",
      "name" : "Classy and Simple Wall Clock",
      "image" : "f2.jpg",
      "images" : ["f2a.jpg","f2b.jpg","f2c.jpg"],
      "summary" : "An enclosed Wall Clock with a face that features both classic roman numerals and numbers.",
      "description": "This piece is elegant and simple with its 5 roses and pine branches hugging the clock face. Perfect for hanging or propping, this one is sure to add understated elegance to any space.",
      "price" : 149.99,
      "discount" : 15,
      "inventory" : 36,
      "details" : ["2 large roses", "3 medium roses", "AAA Battery not included"],
      "size" : " 9.5H x 9.5W x 3D ",
      "category" : "preserved florals",
      "subcategory" : "framed"
    }
  ];

  vm.getTotal = function(){
    var total = 0;
    for(var i=0;i<cartItems.length;i++){
      total += cartItems[i].price;
    }
    return total;
  };
  vm.qvModal = qvModal;

  function qvModal (item){
    $modal.open({
      templateUrl: 'views/qvModal.html',
      controller: ['$modalInstance','cartItems', 'item', QVModalCtrl],
      controllerAs: 'vm',
      resolve:{
        cartItems: function(){return vm.cartItems},
        item: function(){return item;}
      }
    });
  }
}

function QVModalCtrl ($modalInstance, cartItems, item){
  var vm = this;
  vm.tab = "size";
  vm.item = item;
}