'use strict';

angular.module('seattleGrace')
  .controller('ProductList', ProductListCtrl);

ProductListCtrl.$inject = ['$modal'];

function ProductListCtrl($modal){
  var vm = this;
  vm.items = [
    {
      "id": 111,
      "sku" : "111LVSC",
      "name" : "Lavender & Violet Square Centerpiece",
      "image" : "c1.jpg",
      "images" : ["c1a.jpg","c1b.jpg","c1c.jpg"],
      "summary" : "A relaxing lavender centerpiece with flower pot",
      "description": "The colors and style of this centerpiece are sure to bring calm and relaxation to any space. Complete with a matching square porcelain flower pot, this one is not to be missed. ",
      "price" : 119.99,
      "discount" : 10,
      "inventory" : 22,
      "details" : ["1 large rose", "2 medium roses"],
      "size" : " 8H x 7W x 3D ",
      "category" : "preserved florals",
      "subcategory" : "centerpieces",
      "new" : true
    },
    {
      "id": 112,
      "sku" : "112RWBC",
      "name" : "Red Rose Woven Basket Centerpiece",
      "image" : "c2.jpg",
      "images" : ["c2a.jpg","c2b.jpg","c2c.jpg"],
      "summary" : "A traditional arrangement of various roses in a woven basket.",
      "description": "This piece has a open meadow feel to it but remains traditional with the red rose as the focal point.Perfect on a dining table or shelf or outside on a porch or balcony. ",
      "price" : 89.99,
      "discount" : 30,
      "inventory" : 43,
      "details" : ["1 large rose", "3 medium roses"],
      "size" : " 11.5H x 4W x 3.5D ",
      "category" : "preserved florals",
      "subcategory" : "centerpieces"
    },
    {
      "id": 113,
      "sku" : "113RCC",
      "name" : "Round Candle Centerpiece",
      "image" : "c3.jpg",
      "images" : ["c3a.jpg","c3b.jpg","c3c.jpg"],
      "summary" : "A pedestal centerpiece with a candle at its center",
      "description": "The sky blue tones add a heavenly touch to this unique centerpiece. Available with a matching single blue rose in a bowl.",
      "price" : 179.99,
      "discount" : 25,
      "inventory" : 32,
      "details" : ["6 medium roses"],
      "size" : " 12.5H x 8 Diameter ",
      "category" : "preserved florals",
      "subcategory" : "centerpieces"
    },
    {
      "id": 114,
      "sku" : "114OPRC",
      "name" : "Orange and Pastel Pink Rose Rectangular Centerpiece",
      "image" : "c4.jpg",
      "images" : ["c4a.jpg","c4b.jpg","c4c.jpg"],
      "summary" : "A lovely, inspiring centerpiece perfect for any occasion",
      "description": "The matching of colors is superb as they embody a union of the fall and spring seasons. This piece features one large and two small orange roses along with two medium and one small pastel pink rose. The verdant moss is preserved as well with berries and pine cones rounding out the piece. A truly lovely addition to your dining table setting. ",
      "price" : 129.99,
      "discount" : 10,
      "inventory" : 28,
      "details" : ["2 large roses", "4 medium roses"],
      "size" : " 6H x 16W x 2.5D ",
      "category" : "preserved florals",
      "subcategory" : "centerpieces",
      "new" : true
    },
    {
      "id": 121,
      "sku" : "121CSWF",
      "name" : "Classical Style with White Frame",
      "image" : "f1.jpg",
      "images" : ["f1a.jpg","f1b.jpg","f1c.jpg"],
      "summary" : "A 3D potted plant arrangement with refreshing spring hues.",
      "description": "This piece features minty hues and orange and yellow to accent its fresh look. Hang it on the wall or prop it on a shelf, it will surely freshen and lighten the look of any space.",
      "price" : 119.99,
      "discount" : 15,
      "inventory" : 12,
      "details" : ["2 tansy flowers", "3 medium roses"],
      "size" : " 8.5H x 7W x 3D ",
      "category" : "preserved florals",
      "subcategory" : "framed"
    },
    {
      "id": 122,
      "sku" : "122CSWC",
      "name" : "Classy and Simple Wall Clock",
      "image" : "f2.jpg",
      "images" : ["f2a.jpg","f2b.jpg","f2c.jpg"],
      "summary" : "An enclosed Wall Clock with a face that features both classic roman numerals and numbers.",
      "description": "This piece is elegant and simple with its 5 roses and pine branches hugging the clock face. Perfect for hanging or propping, this one is sure to add understated elegance to any space.",
      "price" : 149.99,
      "discount" : 15,
      "inventory" : 36,
      "details" : ["2 large roses", "3 medium roses", "AAA Battery not included"],
      "size" : " 9.5H x 9.5W x 3D ",
      "category" : "preserved florals",
      "subcategory" : "framed"
    },
    {
      "id": 123,
      "sku" : "123MRB",
      "name" : "Monogrammed in Roses Box",
      "image" : "f3.jpg",
      "images" : ["f3a.jpg","f3b.jpg","f3c.jpg"],
      "summary" : "A monogrammed arrangement of roses for that special someone.",
      "description": "This is the perfect gift for that special someone. More personal and unique than any other gift of flowers. Be sure to customize your own now.",
      "price" : 199.99,
      "discount" : 5,
      "inventory" : 1,
      "details" : ["25-40 small roses"],
      "size" : " 8H x 8W x 4D ",
      "category" : "preserved florals",
      "subcategory" : "framed"
    },
    {
      "id" : 131,
      "sku" : "131PBHW",
      "name" : "Purple and Sky Blue Heart Wreath",
      "image" : "w1.jpg",
      "summary" : "A cupid inspired wreath of moss",
      "description": "The incredible attention to detail with the texture of the moss and the arrow like branch as well as the wildberry colors all make this an absolute one of a kind must have wreath. ",
      "price" : 89.99,
      "discount" : 15,
      "inventory" : 7,
      "details" : ["4 ounces of moss"],
      "size" : "Approximately 9 inches in diameter ",
      "category" : "preserved florals",
      "subcategory" : "wreaths",
      "new" : true
    },
    {
      "id" : 132,
      "sku" : "132FGW",
      "name" : "Fresh Greens Wreath",
      "image" : "w2.jpg",
      "summary" : "A fresh and clean wreath embodying spring",
      "description": "This wreath resembles a piece of a secret elven forest. Refreshing, clean, and full of life, it is sure to add freshness and new life to any space.",
      "price" : 89.99,
      "discount" : 15,
      "inventory" : 33,
      "details" : ["3 ounces of lilacs"],
      "size" : " 8H x 7W x 3D ",
      "category" : "preserved florals",
      "subcategory" : "wreaths"
    },
    {
      "id" : 211,
      "sku" : "211SSC",
      "name" : "Strawberry Smoothie Candle",
      "image" : "cnd1.jpg",
      "summary" : "A candle that looks absolutely delicious",
      "description": "This candle was made with all natural soy wax and essential oils. It is sure to please with its tarty scent and delicious texture. ",
      "price" : 29.99,
      "discount" : 10,
      "inventory" : 11,
      "details" : ["6 ounces of Ecosoya CB-Advanced Soy Wax ", "1 ounce vanilla fragrance oil", "1 ounce strawberry fragrance oil"],
      "size" : " 4.5H x 3 Diameter ",
      "category" : "candles",
      "subcategory" : "candles",
      "new" : true
    }
  ];
  vm.filterSelect = "";
  vm.view = "grid";
  vm.tab = "details";
  vm.qvModal = qvModal;

  function qvModal (item){
    $modal.open({
      templateUrl: 'views/qvModal.html',
      controller: ['$modalInstance', 'items', 'item', QVModalCtrl],
      controllerAs: 'vm',
      resolve:{
        items: function(){return vm.items},
        item: function(){return item;}
      }
    });
  }
}

function QVModalCtrl ($modalInstance, items, item){
  var vm = this;
  vm.tab = "details";
  vm.item = item;
}