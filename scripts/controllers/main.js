'use strict';

angular.module('seattleGrace')
  .controller('Main', MainCtrl);

MainCtrl.$inject = ['$modal'];

function MainCtrl($modal){
  var vm = this;
  vm.featuredItems = [
    {
      "id": 111,
      "sku" : "111LVSC",
      "name" : "Lavender & Violet Square Centerpiece",
      "image" : "c1.jpg",
      "summary" : "A relaxing lavender centerpiece with flower pot",
      "description": "The colors and style of this centerpiece are sure to bring calm and relaxation to any space. Complete with a matching square porcelain flower pot, this one is not to be missed. ",
      "price" : 119.99,
      "inventory" : 43,
      "category" : "preserved florals",
      "subcategory" : "centerpieces"
    },
    {
      "id": 123,
      "sku" : "123MRB",
      "name" : "Monogrammed in Roses Box",
      "image" : "f3.jpg",
      "summary" : "A monogrammed arrangement of roses for that special someone.",
      "description": "This is the perfect gift for that special someone. More personal and unique than any other gift of flowers. Be sure to customize your own now.",
      "price" : 199.99,
      "inventory" : 99,
      "category" : "preserved florals",
      "subcategory" : "framed"
    },
    {
      "id" : 132,
      "sku" : "132FGW",
      "name" : "Fresh Greens Wreath",
      "image" : "w2.jpg",
      "summary" : "A fresh and clean wreath embodying spring",
      "description": "This wreath resembles a piece of a secret elven forest. Refreshing, clean, and full of life, it is sure to add freshness and new life to any space.",
      "price" : 89.99,
      "inventory" : 43,
      "category" : "preserved florals",
      "subcategory" : "wreaths"
    },
    {
      "id": 122,
      "sku" : "122CSWC",
      "name" : "Classy and Simple Wall Clock",
      "image" : "f2.jpg",
      "summary" : "An enclosed Wall Clock with a face that features both classic roman numerals and numbers.",
      "description": "This piece is elegant and simple with its 5 roses and pine branches hugging the clock face. Perfect for hanging or propping, this one is sure to add understated elegance to any space.",
      "price" : 149.99,
      "inventory" : 22,
      "category" : "preserved florals",
      "subcategory" : "framed"
    }

    ];
  vm.qvModal = qvModal;

  function qvModal (item){
    $modal.open({
      templateUrl: 'views/qvModal.html',
      controller: ['$modalInstance','featuredItems', 'item', QVModalCtrl],
      controllerAs: 'vm',
      resolve:{
        featuredItems: function(){return vm.featuredItems},
        item: function(){return item;}
      }
    });
  }
}

function QVModalCtrl ($modalInstance, featuredItems, item){
  var vm = this;
  vm.tab = "size";
  vm.item = item;
}