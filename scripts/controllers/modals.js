
angular.module('seattleGrace')
  .controller('QVmodal', QVMController);

QVMController.$inject = ['$modal'];
function QVMController($modal) {
  var vm = this;

  vm.deleteModal = deleteModal;
  vm.people = [
    'Fred',
    'Jim',
    'Bob'
  ];

  function deleteModal(person) {
    $modal.open({
      templateUrl: 'views/modal.html',
      controller: ['$modalInstance', 'people', 'person', DeleteModalCtrl],
      controllerAs: 'vm',
      resolve: {
        people: function () { return vm.people },
        person: function() { return person; }
      }
    });
  }
}

function DeleteModalCtrl($modalInstance, people, person) {
  var vm = this;

  vm.person = person;
  vm.deletePerson = deletePerson;

  function deletePerson() {
    people.splice(people.indexOf(person), 1);
    $modalInstance.close();
  }
}

