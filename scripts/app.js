'use strict';

var seattleGrace = angular.module('seattleGrace', ['ngRoute','ngResource','LocalStorageModule','ui.bootstrap'])
  .config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('ls');
  }])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: 'views/main.html',
        controller: 'Main as vm',
      })
      .when('/productList', {
        templateUrl: 'views/productList.html',
        controller: 'ProductList as vm',
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'About as vm',
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'Contact as vm',
      })
      .when('/account', {
        templateUrl: 'views/account.html',
        controller: 'Account as vm',
      })
     /* .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })*/
      .otherwise({
        redirectTo: '/home'
      });


  })
  ;

angular.module('seattleGrace')
  .controller('About', AboutCtrl)
  .controller('Contact', ContactCtrl)
  .controller('Account', AccountCtrl);
function AboutCtrl(){}
function ContactCtrl(){}
function AccountCtrl(){}
